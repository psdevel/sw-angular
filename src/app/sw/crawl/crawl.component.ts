import { Component, OnInit } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { FilmInterface, SwService } from '@app/sw/sw.service';

@Component({
  selector: 'app-crawl',
  templateUrl: './crawl.component.html',
  styleUrls: ['./crawl.component.scss'],
  providers: [SwService]
})
export class CrawlComponent implements OnInit {

  crawlers: FilmInterface[] = [];

  constructor(
    private swService: SwService
  ) {}

  ngOnInit() {
    this.swService.getData<FilmInterface>(
      '/api/sw/workshop_crawl',
      new HttpParams(),
      undefined
    )
    .subscribe(res => {
      this.crawlers = res.body;
    });
  }
}
