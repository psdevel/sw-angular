import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { isNumber } from 'util';

@Injectable({
  providedIn: 'root'
})
export class SwService {

  constructor(
      private http: HttpClient
  ) { }

  public getData<T>(url: string, params: HttpParams, ids?: number[]): Observable<HttpResponse<T[]>> {
    ids = ids ? ids.filter(id => isNumber(id)) : undefined;
    if (ids === undefined) {
      return this.http.get<T[]>(url, {observe: 'response', params}).pipe(
          map((parameters: HttpResponse<T[]>) => {
            parameters.body.map(param => { param = this.getDependencies(param); });
            return parameters;
          })
      );
    } else if (ids.length > 0) {
      ids.forEach(id => params = params.append('id', id.toString()) );
      return this.http.get<T[]>(url, {observe: 'response', params});
    }
  }

  public getCollectionColumns(table: string): Observable<ColumnInterface[]> {
    return this.http.get<{table: string, columns: ColumnInterface[]}[]>(
        '/api/sw/collection_columns',
        {params: new HttpParams().set('table', table)}
    ).pipe(
        map(collectionColumnsFromApi => {
          return collectionColumnsFromApi[0].columns;
        })
    );
  }

  /**
   * @description Due to Json-Server cannot return nested object, we need to get dependencies by own workaround
   */
  private getDependencies(object): any {
    if (object.hasOwnProperty('characters')) {
      object.characters = this.getData<CharacterInterface>('/api/sw/characters', new HttpParams(), object.charactes as number[]);
    }
    if (object.hasOwnProperty('residents')) {
      object.residents = this.getData<CharacterInterface>('/api/sw/characters', new HttpParams(), object.residents as number[]);
    }
    if (object.hasOwnProperty('people')) {
      object.people = this.getData<CharacterInterface>('/api/sw/characters', new HttpParams(), object.people as number[]);
    }
    if (object.hasOwnProperty('pilots')) {
      object.pilots = this.getData<CharacterInterface>('/api/sw/characters', new HttpParams(), object.pilots as number[]);
    }
    if (object.hasOwnProperty('planets')) {
      object.planets = this.getData<PlanetInterface>('/api/sw/planets', new HttpParams(), object.planets as number[]);
    }
    if (object.hasOwnProperty('homeworld')) {
      object.homeworld = this.getData<PlanetInterface>('/api/sw/planets', new HttpParams(), [object.homeworld] as number[]);
    }
    if (object.hasOwnProperty('films')) {
      object.films = this.getData<FilmInterface>('/api/sw/films', new HttpParams(), object.films as number[]);
    }
    if (object.hasOwnProperty('species')) {
      object.species = this.getData<SpeciesInterface>('/api/sw/species', new HttpParams(), object.species as number[]);
    }
    if (object.hasOwnProperty('starships')) {
      object.starships = this.getData<StarshipInterface>('/api/sw/starships', new HttpParams(), object.starships as number[]);
    }
    if (object.hasOwnProperty('vehicles')) {
      object.vehicles = this.getData<VehicleInterface>('/api/sw/vehicles', new HttpParams(), object.vehicles as number[]);
    }
    return object;
  }
}

export interface CharacterInterface {
  id: number;
  name: string;
  birth_year: string;
  eye_color: string;
  gender: string;
  hair_color: string;
  height: string;
  mass: string;
  skin_color: string;
  homeworld: number|Observable<HttpResponse<PlanetInterface[]>>;
  films: number[]|Observable<HttpResponse<FilmInterface[]>>;
  species: number[]|Observable<HttpResponse<SpeciesInterface[]>>;
  starships: number[]|Observable<HttpResponse<StarshipInterface[]>>;
  vehicles: number[]|Observable<HttpResponse<VehicleInterface[]>>;
  created: string;
  edited: string;
}

export interface FilmInterface {
  id: number;
  title: string;
  episode_id: number;
  opening_crawl: string;
  director: string;
  producer: string;
  release_date: string;
  species: number[]|Observable<HttpResponse<SpeciesInterface[]>>;
  starships: number[]|Observable<HttpResponse<StarshipInterface[]>>;
  vehicles: number[]|Observable<HttpResponse<VehicleInterface[]>>;
  characters: number[]|Observable<HttpResponse<CharacterInterface[]>>;
  planets: number[]|Observable<HttpResponse<PlanetInterface[]>>;
  created: string;
  edited: string;
}

export interface SpeciesInterface {
  id: number;
  name: string;
  classification: string;
  designation: string;
  average_height: string;
  average_lifespan: string;
  eye_colors: string;
  hair_colors: string;
  skin_colors: string;
  language: string;
  homeworld: number;
  people: number[]|Observable<HttpResponse<CharacterInterface[]>>;
  films: number[]|Observable<HttpResponse<FilmInterface[]>>;
  created: string;
  edited: string;
}

export interface StarshipInterface {
  id: number;
  name: string;
  model: string;
  starship_class: string;
  manufacturer: string;
  cost_in_credits: string;
  length: string;
  crew: string;
  passengers: string;
  max_atmosphering_speed: string;
  hyperdrive_rating: string;
  MGLT: string;
  cargo_capacity: string;
  consumables: string;
  films: number[]|Observable<HttpResponse<FilmInterface[]>>;
  pilots: number[]|Observable<HttpResponse<CharacterInterface[]>>;
  created: string;
  edited: string;
}

export interface VehicleInterface {
  id: number;
  name: string;
  model: string;
  vehicle_class: string;
  manufacturer: string;
  length: string;
  cost_in_credits: string;
  crew: string;
  passengers: string;
  max_atmosphering_speed: string;
  cargo_capacity: string;
  consumables: string;
  films: number[]|Observable<HttpResponse<FilmInterface[]>>;
  pilots: number[]|Observable<HttpResponse<CharacterInterface[]>>;
  created: string;
  edited: string;
}

export interface PlanetInterface {
  id: number;
  name: string;
  diameter: string;
  rotation_period: string;
  orbital_period: string;
  gravity: string;
  population: string;
  climate: string;
  terrain: string;
  surface_water: string;
  residents: number[]|Observable<HttpResponse<CharacterInterface[]>>;
  films: number[]|Observable<HttpResponse<FilmInterface[]>>;
  created: string;
  edited: string;
}

export interface ColumnInterface {
  field: string;
  header: string;
  visible: boolean;
  object_field?: string;
  description?: string;
}
