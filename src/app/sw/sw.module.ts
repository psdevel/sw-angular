import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SwRoutingModule } from './sw-routing.module';
import { SwComponent } from './sw.component';
import { TableComponent } from './table/table.component';
import { MenuModule } from '@app/shared/menu/menu.module';
import { SanitizeHtmlPipe } from '@app/sw/pipe/sanitizeHtml.pipe';
import { IntToRomanPipe } from '@app/sw/pipe/intToRoman.pipe';
import { NrToPPipe } from '@app/sw/pipe/nrToP.pipe';
import { HttpClientModule } from '@angular/common/http';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { CrawlComponent } from '@app/sw/crawl/crawl.component';

@NgModule({
  declarations: [
    SwComponent,
    TableComponent,
    SanitizeHtmlPipe,
    IntToRomanPipe,
    NrToPPipe,
    CrawlComponent
  ],
  imports: [
    CommonModule,
    SwRoutingModule,
    MenuModule,
    HttpClientModule,
    TableModule,
    PaginatorModule
  ]
})
export class SwModule { }
