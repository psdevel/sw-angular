import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nrToP'
})
export class NrToPPipe implements PipeTransform {

  constructor() { }

  transform(value: string): string {
    return '<p>' + value.replace(/\r\n\r\n/g, '</p><p>').replace(/\n\n/g, ' ') + '</p>';
  }

}
