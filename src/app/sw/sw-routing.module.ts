import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableComponent } from '@app/sw/table/table.component';
import { SwComponent } from '@app/sw/sw.component';
import { MenuComponent } from '@app/shared/menu/menu.component';
import { CrawlComponent } from '@app/sw/crawl/crawl.component';

const routes: Routes = [
  {path: 'crawls', redirectTo: '', pathMatch: 'full'},
  {path: '', component: SwComponent, children: [
      {path: '', outlet: 'menu', component: MenuComponent, data: {modulePath: '/sw', apiEndpoint: '/api/sw/menu'}},
      {path: '', component: CrawlComponent},
      {path: ':id', component: TableComponent}]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SwRoutingModule { }
