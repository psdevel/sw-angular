import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';
import {
  CharacterInterface, ColumnInterface, FilmInterface, PlanetInterface,
  SpeciesInterface, StarshipInterface, SwService, VehicleInterface
} from '@app/sw/sw.service';
import { HttpParams, HttpResponse } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Table } from 'primeng/table';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  providers: [SwService]
})
export class TableComponent implements OnInit {

  @ViewChild('table', {static: true}) table: Table;

  data: any;
  columns: ColumnInterface[];
  currentRouterParams: Params;

  totalRecords: number;
  rows = 10;
  first = 0;

  constructor(
      private route: ActivatedRoute,
      private swService: SwService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(routeParams => {
      this.currentRouterParams = routeParams;
      this.swService.getCollectionColumns(routeParams.id).subscribe(
          res => this.columns = res
      );
      this.callData(routeParams.id).subscribe(
          res => this.data = res.body
      );
      this.first = 0;
      this.table.expandedRowKeys = {};
    });
  }

  getVisibleColumns(columns: ColumnInterface[]): ColumnInterface[] {
    if (columns) {
      return columns.filter(col => col.visible === true);
    }
  }

  callData(routeParamsId: string): Observable<HttpResponse<any>> {
    const httpParams: HttpParams = new HttpParams()
    .append('_page', (Math.ceil(this.first / this.rows) + 1).toString())
    .append('_limit', this.rows.toString());

    switch (routeParamsId) {
      case 'characters':
        return this.swService.getData<CharacterInterface>('/api/sw/characters', httpParams)
        .pipe(tap(res => this.setTotalRecords(res.headers.get('x-total-count'))));
      case 'vehicles':
        return this.swService.getData<VehicleInterface>('/api/sw/vehicles', httpParams)
        .pipe(tap(res => this.setTotalRecords(res.headers.get('x-total-count'))));
      case 'starships':
        return this.swService.getData<StarshipInterface>('/api/sw/starships', httpParams)
        .pipe(tap(res => this.setTotalRecords(res.headers.get('x-total-count'))));
      case 'species':
        return this.swService.getData<SpeciesInterface>('/api/sw/species', httpParams)
        .pipe(tap(res => this.setTotalRecords(res.headers.get('x-total-count'))));
      case 'planets':
        return this.swService.getData<PlanetInterface>('/api/sw/planets', httpParams)
        .pipe(tap(res => this.setTotalRecords(res.headers.get('x-total-count'))));
      case 'films':
      default:
        return this.swService.getData<FilmInterface>('/api/sw/films', httpParams)
        .pipe(tap(res => this.setTotalRecords(res.headers.get('x-total-count'))));
    }
  }

  setTotalRecords(xTotalCount: string) {
    this.totalRecords = parseInt(xTotalCount, 10);
  }

  isObservable(object: any): boolean {return object instanceof Observable;}

  tableChangeTriggered(event) {
    this.first = event.first;
    this.rows = event.rows;
    this.callData(this.currentRouterParams.id).subscribe(res => this.data = res.body);
  }

  getValue(array: any[], column: ColumnInterface): any[] {
    return array.map(item => {
      return item[column.object_field];
    });
  }


}
