import { Component, OnInit } from '@angular/core';
import { MenuItemInterface, MenuService } from '@app/shared/menu/menu.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  providers: [MenuService]
})
export class MenuComponent implements OnInit {

  public menuItems: MenuItemInterface[];
  public routeData: {modulePath: string, apiEndpoint: string};

  constructor(private menuService: MenuService, private route: ActivatedRoute) {
    this.route.data.subscribe(
        (res: {modulePath: string, apiEndpoint: string}) => this.routeData = res
    );
  }

  ngOnInit() {
    this.menuService.getMenu(this.routeData.apiEndpoint).subscribe(
        res => this.menuItems = res
    );
  }
}
