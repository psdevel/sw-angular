import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class MenuService {

  constructor(
      private http: HttpClient,
  ) { }

  public getMenu(url: string): Observable<MenuItemInterface[]> {
    return this.http.get<MenuItemInterface[]>(url);
  }
}

export interface MenuItemInterface {
  id: number;
  label: string;
  parameter: string;
}
